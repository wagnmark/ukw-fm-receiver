use num::complex::Complex;
use pulse::sample::{Format, Spec};
use pulse::stream::Direction;
use rtlsdr_mt::{devices, open};
use spulse::Simple;
use std::sync::mpsc;
use std::vec::Vec;
use sdr::fir::FIR;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

fn main() {
    for dev in devices() {
        println!("device: {:?}", dev);
    }
    // get sdr control
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    ctrlc::set_handler(move || {
        println!("shutting down...");
        r.store(false, Ordering::SeqCst);
    }).unwrap();
    let (mut ctl, mut reader) = open(0).unwrap();
    ctl.cancel_async_read();
    ctl.set_center_freq(93_700_000).unwrap();
    ctl.set_sample_rate(240_000).unwrap();
    ctl.enable_agc().unwrap();
    //ctl.set_tuner_gain(469).unwrap();
    let (iq_tx, iq_rx) = mpsc::channel();
    let (dtphase_tx, dtphase_rx) = mpsc::channel();
    let read_th = std::thread::spawn(move || {
        reader
            .read_async(4, 65536, |bytes| {
                let iq_len = bytes.len() / 2;
                let mut iq_data = Vec::with_capacity(iq_len);
                for idx in 0..iq_len {
                    iq_data.push(Complex::new(
                        bytes[idx * 2] as f32 / 128f32 - 1f32,
                        bytes[idx * 2 + 1] as f32 / 128f32 - 1f32,
                    ))
                }
                iq_tx.send(iq_data).expect("error sending iq raw data");
            })
            .expect("error during reading iq data from sdr");
    });
    let demod_th = std::thread::spawn(move || {
        // prepare downsampling by factor 5 to 48kHz; cutoff freq 15kHz
        let mut downsampling_filter = FIR::<f32>::lowpass(32, 0.0625);
        let mut iq_old = Complex::new(0f32, 0f32);
        loop {
            if let Ok(iq_samples) = iq_rx.recv() {
                let mut dt_phase = Vec::with_capacity(iq_samples.len());
                for iq in iq_samples {
                    let iq_sample = iq.scale(1f32 / iq.norm());
                    //let iq_sample = iq;
                    dt_phase.push((iq_sample * iq_old.conj()).arg());
                    iq_old = iq_sample;
                }
                let dt_phase_filtered = downsampling_filter.process(dt_phase.as_slice());
                dtphase_tx.send(dt_phase_filtered).unwrap();
                //dtphase_tx.send(dt_phase).unwrap();
            } else {
                break;
            }
        }
    });
    let audio_th = std::thread::spawn(move || {
        let mut down_sampl_cnt = 0;
        let spec = Spec {
            format: Format::U8,
            channels: 1,
            rate: 48_000,
        };
        let s = Simple::new(
            None,
            "FM Radio",
            Direction::Playback,
            None,
            "Music",
            &spec,
            None,
            None,
        )
        .unwrap();
        loop {
            if let Ok(dtphase) = dtphase_rx.recv() {
                let mut audio_buf = Vec::with_capacity(dtphase.len()/5);
                for sample in dtphase {
                    if down_sampl_cnt == 0 {
                        let u8_sample: u8 = (sample * 80f32 + 128f32) as u8;
                        audio_buf.push(u8_sample);
                    }
                    down_sampl_cnt = (down_sampl_cnt + 1) % 5;
                }
                s.write(audio_buf.as_slice()).unwrap();
            } else {
                break;
            }
        }
        s.drain().unwrap();
    });
    while running.load(Ordering::SeqCst) {
        std::thread::sleep(std::time::Duration::from_millis(500));
    };
    let mut gains = [0i32;32];
    ctl.tuner_gains(&mut gains);
    println!("gains: {:?}",gains);
    ctl.cancel_async_read();
    println!("reading canceled");
    read_th.join().expect("read thread paniced!");
    demod_th.join().expect("demod thread paniced!");
    audio_th.join().expect("audio thread paniced");
}
